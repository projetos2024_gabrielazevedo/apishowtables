// Obtendo a conexão com o banco de dados
const connect = require("../db/connect");

module.exports = class clienteController {
  static async createCliente(req, res) {
    const {
      telefone,
      nome,
      cpf,
      logradouro,
      numero,
      complemento,
      bairro,
      cidade,
      estado,
      cep,
      referencia,
    } = req.body;

    // Verificando se o atributo chave é diferente de 0 (zero)
    if (telefone !== 0) {
      const query = `insert into cliente( telefone, nome, cpf, logradouro, numero, complemento, bairro, cidade, estado, cep, referencia) values (
                '${telefone}',
                '${nome}',
                '${cpf}',
                '${logradouro}',
                '${numero}',
                '${complemento}',
                '${bairro}',
                '${cidade}',
                '${estado}',
                '${cep}',
                '${referencia}'

            )`; // Fim da query

      try {
        connect.query(query, function (err) {
          if (err) {
            console.log(err);
            res
              .status(500)
              .json({ error: "Usuário não cadastrado no banco!!!" });
            return;
          }
          console.log("Inserido no Banco!!!");
          res.status(201).json({ message: "Usuário criado com sucesso!!!" });
        });
      } catch (error) {
        console.error("Erro ao executar o insert!!!", error);
        res.status(500).json({ error: "Erro interno do servidor!!!" });
      } // Fim do catch
    } // Fim do if
    else {
      res.status(400).json({ message: "O telefone é obrigatório" });
    }
  } // Fim do createCliente

  // Select da tabela cliente
  static async getAllClientes(req, res) {
    const query = `select * from cliente`;

    try {
      connect.query(query, function (err, data) {
        if (err) {
          console.log(err);
          res
            .status(500)
            .json({ error: "Usuários não encontrados no banco!!!" });
          return;
        } // Fim do if
        let clientes = data;
        console.log("Consulta realizada com sucesso!!!");
        res.status(201).json({ clientes });
      }); // Fim do connect query
    } catch (error) {
      console.error("Erro ao executar a consulta: ", error);
      res.status(500).json({ error: "Erro interno do servidor!!!" });
    } // Fim do catch
  } // Fechamento getAllClientes

  static async getAllClientes2(req, res) {
    // Método para selecionar clientes via parâmetros específicos

    // Extrair parâmetros da consulta da URL
    const { filtro, ordenacao, ordem } = req.query;

    // Construir a consulta SQL base
    let query = `select * from cliente`;

    // Adicionar a claúsula where, quando houver
    if (filtro) {
      //query = query + filtro; - está incorreto
      //query = query + ` where ${filtro}`;
      query += ` where ${filtro}`;
    }

    // Adicionar a cláusula order by, quando houver
    if(ordenacao){
      query += `order by ${ordenacao}`;

      // Adicionar a ordem do order by (asc ou desc)
      if (ordem){
        query += ` ${ordem}`;
      } //Fim do if

    }// Fim do if ordenação

    try {
      connect.query(query, function (err, result) {
        if (err) {
          console.log(err);
          res
            .status(500)
            .json({ error: "Usuários não encontrados no banco!!!" });
          return;
        } // Fim do if
        console.log("Consulta realizada com sucesso!!!");
        res.status(201).json({ result });
      }); // Fim do connect query
    } catch (error) {
      console.error("Erro ao executar a consulta: ", error);
      res.status(500).json({ error: "Erro interno do servidor!!!" });
    } // Fim do catch
  } // Fechamento getAllClientes2
}; // Fim do module
