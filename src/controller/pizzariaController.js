const connect = require("../db/connect");

module.exports = class pizzariaController{
    static async listarPedidosPizza(req, res){
        const query = `
            select 
                pp.fk_id_pedido as Pedido, 
                p.nome as Pizza,
                pp.quantidade as Qtde,
                round((pp.valor / pp.quantidade), 2) as unitário,
                pp.valor as Total 
            from 
                pizza_pedido pp,
                pizza p
            where
                pp.fk_id_pizza = p.id_pizza
            order by 
                pp.fk_id_pedido;
        `;// Fim da atribuição da constante

        try{
            connect.query(query, function(err, result){
                if(err){
                    console.log(err);
                    return res.status(500).json({error: "Erro ao consultar o banco de dados!!!"})
                }

                console.log("Consulta realizada com sucesso!!!");
                return res.status(200).json({result});
            });// Fim do connect.query

        }catch(error){
            console.error("Erro ao executar a consultade pedidos de pizzas: ", error);
            return res.status(500).json({error: "Erro interno do servidor!!!"});
        }// Fim do catch


    }// Fim do listasPedidosPizza

};// Fim do module exports