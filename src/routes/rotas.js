const router = require("express").Router();
const dbController = require("../controller/dbController")
const clienteController = require("../controller/clienteController");
const pizzariaController = require("../controller/pizzariaController");

// Rotas para consulta das tabelas do banco
router.get("/tables/getAllTableDescriptions", dbController.getAllTableDescriptions)
router.get("/tables/getAllTableNames", dbController.getAllTableNames)

// Rota para cadastro de clientes
router.post("/postcliente/", clienteController.createCliente);

// Rota para select da tabela cliente
router.get("/clientes/", clienteController.getAllClientes);

// Rota para select da tabela cliente com filtro
router.get("/clientes2", clienteController.getAllClientes2); 

// Rota para listar as pizzas pedidas
router.get("/listarPizzaPedido/", pizzariaController.listarPedidosPizza);


module.exports = router;
